//CRUD
/*
	
	C -  Create (Insert document)
	R - Read (View specific document)
	U - Update (Edit specific document)
	D - Delete (Remove specific document)

	-CRUD Operations are the heart of any backend application.

*/

//Create
db.users.insertOne({
	firstName: "Jane",
	lastName: "Joe",
	age: 21,
	contact: {
		phone: "12345",
		email: "janedoe@gmail.com"
	},
	courses: ["CSS", "JavaScript", "Python"],
	department: "none"
});


db.rooms.insertOne(
	{
		name: "single",
		accomodates: 2,
		price: 1000,
		description: "A simple room with basic necessities",
		rooms_available: 10,
		isAvailable: false
	}
);


//Insert Many
db.users.insertMany([
	{
		firstName: "Stephen",
		lastName: "Hawking",
		age: 76,
		contact: {
			phone: "87654321",
			email: "stephenhawking@gmail.com"
		},
		courses: ["Python", "React", "PHP"],
		department: "none"
	},
	{
		firstName: "Neil",
		lastName: "Armstrong",
		age: 82,
		contact: {
			phone: "87654321",
			email: "neilarmstrong@gmail.com"
		},
		courses: ["React", "Laravel", "Sass"],
		department: "none"
	}
]);

db.rooms.insertMany([
	{
		name: "double",
		accomodates: 3,
		price: 2000,
		description: "A room fit for a small family going on a vacation",
		rooms_available: 5,
		isAvailable: false
	},
	{
		name: "queen",
		accomodates: 4,
		price: 4000,
		description: " A room with a queen sized bed perfect for a simple getaway",
		rooms_available: 15,
		isAvailable: false
	}
]);


//Read
db.users.find({});

db.users.find(
		{
		 firstName: "Stephen"
		}
	);

db.users.find(
		{
		 department: "none"
		}
	);

db.users.find(
		{
		 firstName: "Stephen",
		 age: 82
		}
	);

//Update

db.users.insertOne({
	firstName: "Test",
	lastName: "Test",
	age: 0,
	contact: {
		phone: "000000",
		email: "test@gmail.com"
	},
	courses: [],
	department: "none"
});

db.users.updateOne(
		{
			firstName: "Test"
		},
		{
				$set:{
					firstName: "Bill",
					lastName: "Gates",
					age: 65,
					contact:{
						phone:"12345678",
						email: "bill@gmail.com"
					},
					course: ["PHP", "Laravel", "HTML"],
					department: "Operations",
					status: "active"
				}
		}
	);

db.users.updateMany(
		{
			department: "none"
		},
		{
				$set:{
					department: "HR"
				}
		}
	);

// Replace One
	/*
		- Can be used if replacing the whole document if necessary.
		-Syntax:
		 - db.collectionName.replaceOne({criteria}, {field: value});

	*/

	db.users.replaceOne(
		{firstName: "Bill"},
		{
			firstName: "Bill",
			lastName: "Gates",
			age: 65,
			contact: {
				phone: "12345678",
				email: "bill@gmail.com"
			},
			courses: ["PHP", "Laravel", "HTML"],
			department: "Operations"
		}
	);

/*
    Mini Activity:

        1. Using the hotel database, update the queen room and set the available rooms to zero.

        2. Use the find query to validate if the room is successfully updated.

        3. Take a screenshot of the Robo3t result and send it to the batch hangouts.

*/

db.rooms.updateOne(
		{name:"queen"}
		{
			$set{rooms_available: 0}
		}
	);

//Delete

db.users.deleteOne(
	{
		firstName: "Test"
	});

db.users.deleteMany({
	firstName: "Test"
});


db.rooms.deleteMany({
	rooms_available: 0
});


db.users.find({
	firstName: "Bill"
})

db.users.find({
	courses: ["CSS", "JavaScript", "Python"]
})

db.users.insertOne({
	nameArr:[{
            nameA: "Juan"},
            {
                nameB: "Tamad"
            }]
});

db.users.find({
	nameArr:{nameA: "Juan"}
})